import React, { Component } from "react";

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.name = { N: "John" };
    this.age = 30;
    this.state = { N: "John" };
  }

  static getDerivedStateFromProps(props, state) {
    return { N: props.name };
  }

  componentDidMount() {
    setTimeout(() => {
      this.age = 40;
      document.getElementById("agechange").innerHTML =
        "Age (Changed) : " + this.age;
    }, 6000);
  }

  render() {
    return (
      <div>
        <h1> Phase 1: Mounting</h1>
        <h2>{"Name (Intial) : " + this.name.N}</h2>
        <h2>{"Name (State) : " + this.state.N}</h2>
        <h2 id="agechange">{"Age (Intial) : " + this.age}</h2>
        <h2>{"Age (Intial) : " + this.age}</h2>
      </div>
    );
  }
}
