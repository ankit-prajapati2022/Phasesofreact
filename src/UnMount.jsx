import React from "react";
import Child from "./Child";

export default class UnMount extends React.Component {
  constructor(props) {
    super(props);
    this.state = { show: true };
  }

  delHeader = () => {
    this.setState({ show: false });
  };

  render() {
    let display;
    if (this.state.show) {
      display = <Child />;
    }
    return (
      <div>
        <h1> Phase 3: UnMounting</h1>
        <button type="button" onClick={this.delHeader}>
          Delete Components
        </button>
        {display}
      </div>
    );
  }
}
