import React from "react";

export default class Updating extends React.Component {
  constructor(props) {
    super(props);
    this.product = { name: "Abc", price: 100 };
    this.state = { name: "Abc", price: 100 };
  }

  // static getDerivedStateFromProps(props, state) {
  //   return { name: props.name };
  // }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.getElementById("before").innerHTML =
      "Name (Before Snap) : " + prevState.name;
  }

  componentDidUpdate() {
    document.getElementById("after").innerHTML =
      "Name (After snap) : " + this.state.name;
  }

  componentDidMount() {
    setTimeout(() => {
      this.product = { name: "Xyz", price: 200 };
      document.getElementById("pricechange").innerHTML =
        "Price (Changed) : " + this.product.price;
    }, 6000);
  }

  changeName = () => {
    this.setState({ name: "Xyz" });
  };

  render() {
    return (
      <div>
        <h1></h1>
        <h1> Phase 2: Updating</h1>
        <button type="button" onClick={this.changeName}>
          Change Name
        </button>

        <h2>{"Name (Intial) : " + this.product.name}</h2>
        <h2>{"Name (State) : " + this.state.name}</h2>
        <h2 id="pricechange">{"Price (Intial) : " + this.product.price}</h2>
        <h2>{"Price (Intial) : " + this.product.price}</h2>
        <h2 id="before"></h2>
        <h2 id="after"></h2>
      </div>
    );
  }
}
