import React from "react";
import Mounting from "./Mounting";
import Updating from "./Updating";
import UnMount from "./UnMount";

function App() {
  return (
    <div className="App">
      <Mounting name="Dave" />
      <Updating name="MNO" />
      <UnMount />
    </div>
  );
}

export default App;
